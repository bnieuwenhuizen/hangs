
Small tool to trigger hangs with radv, usable for recovery testing.

## Building

One can build this tool with Meson:

```
mkdir build
meson build
ninja -C build
```

Prerequisites:

- Meson
- libvulkan
- vulkan-headers
- glslangValidator
- A C++11 capable C++ compiler

## Deployment & Running

If you want to run from a different device, copy

- `hang`
- `*.spv`

from the build dir to the device. Then one can run the command from the directory that contains all the spv files. To list all the ways in which it can try to hang the GPU run

```
hang list
```

each line will be a method, which can be executed as e.g.

```
hang event_wait
```

for the `event_wait` method.




