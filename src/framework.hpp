/*
 * Copyright © 2021 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#pragma once

#include <vulkan/vulkan.h>

#include <string>
#include <vector>

struct MemoryWrapper {
   VkDeviceMemory mem;
   unsigned type;
   std::size_t size;
};

enum class Access { gpu, host };

class Device {
 public:
   Device();
   ~Device();

   VkDevice dev()
   {
      return device_;
   }
   VkPhysicalDevice pdev()
   {
      return phys_device_;
   }

   VkQueue gfx_queue();
   unsigned gfx_queue_index();
   VkCommandPool gfx_pool();

   MemoryWrapper alloc_mem(std::size_t size, unsigned type_mask, Access access);
   void free_mem(MemoryWrapper mem);

   VkShaderModule compile_shader(VkShaderStageFlagBits stage, const std::string &filename);

 private:
   void cleanup();

   VkInstance instance_ = VK_NULL_HANDLE;
   VkPhysicalDevice phys_device_ = VK_NULL_HANDLE;
   VkDevice device_ = VK_NULL_HANDLE;

   std::vector<VkQueueFamilyProperties> queue_properties_;
   std::vector<VkQueue> queues_;
   std::vector<VkCommandPool> cmd_pools_;

   VkPhysicalDeviceMemoryProperties memory_props_;

   std::vector<VkShaderModule> shader_modules_;
};

class Image {
 public:
   Image(Device &dev, const VkImageCreateInfo &info, Access access);
   ~Image();

   VkImage image()
   {
      return image_;
   }
   VkImageView view()
   {
      return view_;
   }

   void *data()
   {
      return data_;
   }
   std::size_t mem_size()
   {
      return mem_.size;
   }

 private:
   void cleanup();

   Device *dev_;
   VkImage image_;
   MemoryWrapper mem_;
   VkImageView view_;
   void *data_;
};

class Buffer {
 public:
   Buffer(Device &dev, std::size_t size, Access access);
   ~Buffer();

   VkBuffer buffer()
   {
      return buffer_;
   }

   void *data()
   {
      return data_;
   }
   std::size_t mem_size()
   {
      return mem_.size;
   }

 private:
   void cleanup();

   Device *dev_;
   VkBuffer buffer_;
   MemoryWrapper mem_;
   void *data_;
};

VkCommandBuffer create_cmdbuf(Device &dev, VkCommandPool pool);
void destroy_cmdbuf(Device &dev, VkCommandPool pool, VkCommandBuffer buf);

VkPipeline create_compute_pipeline(Device &dev, VkPipelineLayout layout,
                                   const std::string &filename);
