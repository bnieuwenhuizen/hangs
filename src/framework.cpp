/*
 * Copyright © 2021 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.hpp"

#include <cstdint>
#include <fstream>
#include <vector>

Device::Device()
{
   try {
      VkResult result = VK_SUCCESS;
      const VkApplicationInfo app_info = {
         .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
         .apiVersion = VK_MAKE_VERSION(1, 2, 0),
      };
      const VkInstanceCreateInfo instance_create_info = {
         .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
         .pApplicationInfo = &app_info

      };
      result = vkCreateInstance(&instance_create_info, nullptr, &instance_);
      if (result != VK_SUCCESS)
         throw result;

      std::uint32_t count = 1;
      result = vkEnumeratePhysicalDevices(instance_, &count, &phys_device_);
      if ((result != VK_SUCCESS && result != VK_INCOMPLETE) || !count)
         throw result;

      count = 0;
      vkGetPhysicalDeviceQueueFamilyProperties(phys_device_, &count, NULL);
      queue_properties_.resize(count);
      vkGetPhysicalDeviceQueueFamilyProperties(phys_device_, &count, queue_properties_.data());

      vkGetPhysicalDeviceMemoryProperties(phys_device_, &memory_props_);

      std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
      const float prio[1] = {0.5f};
      for (unsigned i = 0; i < queue_properties_.size(); ++i) {
         queue_create_infos.push_back(VkDeviceQueueCreateInfo{
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, nullptr, 0, i, 1, prio});
      }

      const char *extensions[] = {"VK_KHR_external_memory_fd"};
      const VkDeviceCreateInfo dev_create_info = {
         .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
         .queueCreateInfoCount = (unsigned)queue_create_infos.size(),
         .pQueueCreateInfos = queue_create_infos.data(),
         .enabledExtensionCount = sizeof(extensions) / sizeof(extensions[0]),
         .ppEnabledExtensionNames = extensions};
      result = vkCreateDevice(phys_device_, &dev_create_info, nullptr, &device_);
      if (result != VK_SUCCESS)
         throw -1;

      queues_.resize(queue_create_infos.size());
      cmd_pools_.resize(queues_.size());
      for (unsigned i = 0; i < queues_.size(); ++i) {
         vkGetDeviceQueue(device_, i, 0, &queues_[i]);

         const VkCommandPoolCreateInfo cmd_pool_create_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
            .queueFamilyIndex = i,
         };
         result = vkCreateCommandPool(device_, &cmd_pool_create_info, nullptr, &cmd_pools_[i]);
         if (result != VK_SUCCESS)
            throw result;
      }

   } catch (...) {
      cleanup();
      throw;
   }
}

Device::~Device()
{
   cleanup();
}

void
Device::cleanup()
{
   for (auto e : shader_modules_)
      vkDestroyShaderModule(device_, e, nullptr);
   for (auto e : cmd_pools_)
      if (e)
         vkDestroyCommandPool(device_, e, nullptr);
   if (device_ != VK_NULL_HANDLE)
      vkDestroyDevice(device_, nullptr);
   if (instance_ != VK_NULL_HANDLE)
      vkDestroyInstance(instance_, nullptr);
}

VkQueue
Device::gfx_queue()
{
   return queues_[gfx_queue_index()];
}

unsigned
Device::gfx_queue_index()
{
   for (unsigned i = 0; i < queues_.size(); ++i)
      if (queue_properties_[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
         return i;
   throw -1;
}

VkCommandPool
Device::gfx_pool()
{
   return cmd_pools_[gfx_queue_index()];
}

MemoryWrapper
Device::alloc_mem(std::size_t size, unsigned type_mask, Access access)
{
   VkMemoryPropertyFlags properties;
   switch (access) {
   case Access::gpu:
      properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
      break;
   case Access::host:
      properties = VK_MEMORY_PROPERTY_HOST_CACHED_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT |
                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
      break;
   };
   for (unsigned i = 0; i < memory_props_.memoryTypeCount; ++i) {
      if (!((1u << i) & type_mask))
         continue;

      if (~memory_props_.memoryTypes[i].propertyFlags & properties)
         continue;

      VkDeviceMemory mem;
      const VkMemoryAllocateInfo info = {
         .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
         .allocationSize = size,
         .memoryTypeIndex = i,
      };
      VkResult result = vkAllocateMemory(device_, &info, nullptr, &mem);
      if (result != VK_SUCCESS)
         throw -1;
      return {mem, i, size};
   }
   throw -1;
}

void
Device::free_mem(MemoryWrapper mem)
{
   vkFreeMemory(device_, mem.mem, nullptr);
}

VkShaderModule
Device::compile_shader(VkShaderStageFlagBits stage, const std::string &filename)
{

   std::vector<std::uint32_t> data;
   {
      std::ifstream in(filename);
      in.seekg(0, std::ios::end);
      data.resize(in.tellg() / sizeof(std::uint32_t));
      in.seekg(0, std::ios::beg);
      in.read((char *)data.data(), data.size() * sizeof(std::uint32_t));
   }
   if (!data.size())
      throw -1;
   const VkShaderModuleCreateInfo module_info = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .codeSize = data.size() * sizeof(std::uint32_t),
      .pCode = data.data()};
   VkShaderModule module;
   VkResult result = vkCreateShaderModule(device_, &module_info, nullptr, &module);
   if (result != VK_SUCCESS)
      throw result;
   shader_modules_.push_back(module);
   return module;
}

Image::Image(Device &dev, const VkImageCreateInfo &info, Access access)
    : dev_{&dev}, image_{VK_NULL_HANDLE}, mem_{VK_NULL_HANDLE, 0}, view_{VK_NULL_HANDLE}
{
   try {
      VkResult result = vkCreateImage(dev_->dev(), &info, nullptr, &image_);
      if (result != VK_SUCCESS)
         throw -1;

      VkMemoryRequirements reqs;
      vkGetImageMemoryRequirements(dev_->dev(), image_, &reqs);

      mem_ = dev_->alloc_mem(reqs.size, reqs.memoryTypeBits, access);

      const VkImageViewCreateInfo view_info = {
         .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
         .image = image_,
         .viewType = VK_IMAGE_VIEW_TYPE_2D,
         .format = info.format,
         .subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, info.mipLevels, 0, info.arrayLayers},
      };

      vkBindImageMemory(dev_->dev(), image_, mem_.mem, 0);

      result = vkCreateImageView(dev_->dev(), &view_info, nullptr, &view_);
      if (result != VK_SUCCESS)
         throw -1;

      if (access == Access::host) {
         result = vkMapMemory(dev_->dev(), mem_.mem, 0, mem_.size, 0, &data_);
         if (result != VK_SUCCESS)
            throw -1;
      }
   } catch (...) {
      cleanup();
      throw -1;
   }
}

Image::~Image()
{
   cleanup();
}

void
Image::cleanup()
{
   if (view_ != VK_NULL_HANDLE)
      vkDestroyImageView(dev_->dev(), view_, nullptr);
   if (image_ != VK_NULL_HANDLE)
      vkDestroyImage(dev_->dev(), image_, nullptr);
   if (mem_.mem)
      dev_->free_mem(mem_);
}

Buffer::Buffer(Device &dev, std::size_t size, Access access)
    : dev_{&dev}, buffer_{VK_NULL_HANDLE}, mem_{VK_NULL_HANDLE, 0}
{
   try {
      const VkBufferCreateInfo info = {
         .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
         .size = size,
         .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                  VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
                  VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
                  VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
         .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      };
      VkResult result = vkCreateBuffer(dev_->dev(), &info, nullptr, &buffer_);
      if (result != VK_SUCCESS)
         throw -1;

      VkMemoryRequirements reqs;
      vkGetBufferMemoryRequirements(dev_->dev(), buffer_, &reqs);

      mem_ = dev_->alloc_mem(reqs.size, reqs.memoryTypeBits, access);

      vkBindBufferMemory(dev_->dev(), buffer_, mem_.mem, 0);

      if (access == Access::host) {
         result = vkMapMemory(dev_->dev(), mem_.mem, 0, mem_.size, 0, &data_);
         if (result != VK_SUCCESS)
            throw -1;
      }
   } catch (...) {
      cleanup();
      throw -1;
   }
}

Buffer::~Buffer()
{
   cleanup();
}

void
Buffer::cleanup()
{
   if (buffer_ != VK_NULL_HANDLE)
      vkDestroyBuffer(dev_->dev(), buffer_, nullptr);
   if (mem_.mem)
      dev_->free_mem(mem_);
}

VkCommandBuffer
create_cmdbuf(Device &dev, VkCommandPool pool)
{
   VkCommandBuffer cmdbuf;
   const VkCommandBufferAllocateInfo info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1,
   };
   VkResult result = vkAllocateCommandBuffers(dev.dev(), &info, &cmdbuf);
   if (result != VK_SUCCESS)
      throw -1;

   return cmdbuf;
}

void
destroy_cmdbuf(Device &dev, VkCommandPool pool, VkCommandBuffer buf)
{
   vkFreeCommandBuffers(dev.dev(), pool, 1, &buf);
}

VkPipeline
create_compute_pipeline(Device &dev, VkPipelineLayout layout, const std::string &filename)
{
   VkShaderModule module = dev.compile_shader(VK_SHADER_STAGE_COMPUTE_BIT, filename);
   if (!module)
      throw -1;
   VkComputePipelineCreateInfo create_info = {
      .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      .stage = {.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                .stage = VK_SHADER_STAGE_COMPUTE_BIT,
                .module = module,
                .pName = "main"},
      .layout = layout,
   };

   VkPipeline pipeline = VK_NULL_HANDLE;
   VkResult result =
      vkCreateComputePipelines(dev.dev(), VK_NULL_HANDLE, 1, &create_info, NULL, &pipeline);
   if (result != VK_SUCCESS)
      throw -1;
   return pipeline;
}
