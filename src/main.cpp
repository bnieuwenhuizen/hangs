/*
 * Copyright © 2021 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "framework.hpp"

#include <cstring>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

namespace {

void
infinite_loop(int wg_count)
{
   Device device;
   VkResult result;

   const VkDescriptorSetLayoutBinding bindings[] = {{
      .binding = 0,
      .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
   }};
   const VkDescriptorSetLayoutCreateInfo ds_layout_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = 1,
      .pBindings = bindings};
   VkDescriptorSetLayout ds_layout;
   result = vkCreateDescriptorSetLayout(device.dev(), &ds_layout_info, NULL, &ds_layout);
   if (result != VK_SUCCESS)
      throw -1;

   VkPipelineLayout pl;
   const VkPipelineLayoutCreateInfo pl_create_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 1,
      .pSetLayouts = &ds_layout,
      .pushConstantRangeCount = 0,
   };
   result = vkCreatePipelineLayout(device.dev(), &pl_create_info, nullptr, &pl);
   if (result != VK_SUCCESS)
      throw -1;

   auto pipeline = create_compute_pipeline(device, pl, "infinite.spv");

   VkDescriptorPoolSize ds_pool_sizes[] = {
      {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 128},  {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 128},
      {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 128},  {VK_DESCRIPTOR_TYPE_SAMPLER, 128},
      {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 128}, {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 128},
   };
   VkDescriptorPoolCreateInfo ds_pool_create_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
      .maxSets = 128,
      .poolSizeCount = 6,
      .pPoolSizes = ds_pool_sizes};

   VkDescriptorPool ds_pool;
   result = vkCreateDescriptorPool(device.dev(), &ds_pool_create_info, NULL, &ds_pool);
   if (result != VK_SUCCESS)
      throw -1;

   VkDescriptorSetAllocateInfo ds_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
      .descriptorPool = ds_pool,
      .descriptorSetCount = 1,
      .pSetLayouts = &ds_layout,
   };
   VkDescriptorSet ds;
   result = vkAllocateDescriptorSets(device.dev(), &ds_info, &ds);
   if (result != VK_SUCCESS)
      throw -1;

   VkCommandBuffer cmd_buf = create_cmdbuf(device, device.gfx_pool());
   const VkCommandBufferBeginInfo begin_info = {.sType =
                                                   VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
   vkBeginCommandBuffer(cmd_buf, &begin_info);

   vkCmdBindPipeline(cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
   vkCmdBindDescriptorSets(cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pl, 0, 1, &ds, 0, NULL);
   vkCmdDispatch(cmd_buf, wg_count, 1, 1);
   if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
      throw -1;

   VkFence fence;
   const VkFenceCreateInfo fence_info = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
      .flags = 0,
   };
   result = vkCreateFence(device.dev(), &fence_info, nullptr, &fence);
   if (result != VK_SUCCESS)
      throw -1;

   const VkSubmitInfo submit_info = {.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                     .commandBufferCount = 1,
                                     .pCommandBuffers = &cmd_buf};
   vkQueueSubmit(device.gfx_queue(), 1, &submit_info, fence);
}

void
wait_event()
{
   Device device;
   VkResult result;

   VkEvent event;
   const VkEventCreateInfo event_info = {
      .sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO,
   };
   result = vkCreateEvent(device.dev(), &event_info, NULL, &event);
   if (result != VK_SUCCESS)
      throw -1;

   VkCommandBuffer cmd_buf = create_cmdbuf(device, device.gfx_pool());
   const VkCommandBufferBeginInfo begin_info = {.sType =
                                                   VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
   vkBeginCommandBuffer(cmd_buf, &begin_info);

   vkCmdWaitEvents(cmd_buf, 1, &event, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                   VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, NULL, 0, NULL, 0, NULL);
   if (vkEndCommandBuffer(cmd_buf) != VK_SUCCESS)
      throw -1;

   VkFence fence;
   const VkFenceCreateInfo fence_info = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
      .flags = 0,
   };
   result = vkCreateFence(device.dev(), &fence_info, nullptr, &fence);
   if (result != VK_SUCCESS)
      throw -1;

   const VkSubmitInfo submit_info = {.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                     .commandBufferCount = 1,
                                     .pCommandBuffers = &cmd_buf};
   vkQueueSubmit(device.gfx_queue(), 1, &submit_info, fence);
}

} // namespace

int
main(int argc, char *argv[])
{
   std::vector<std::pair<std::string, std::function<void()>>> handlers = {
      {"infinite_loop", [] { infinite_loop(1); }},
      {"wait_event", [] { wait_event(); }},
   };

   if (argc != 2) {
      std::cerr << "usage: hang [ method | list ]\nmethods:\n";
      for (auto &&h : handlers)
         std::cerr << "  " << h.first << "\n";
      return 1;
   }
   if (std::strcmp(argv[1], "list") == 0) {
      for (auto &&h : handlers)
         std::cerr << h.first << "\n";
      return 0;
   }

   for (auto &&h : handlers) {
      if (h.first == argv[1]) {
         h.second();
         return 0;
      }
   }
   std::cerr << "method not found: \"" << argv[1] << "\"\n";
   return 1;
}
